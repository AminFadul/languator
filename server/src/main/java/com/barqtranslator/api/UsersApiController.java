package com.barqtranslator.api;

import com.barqtranslator.core.Status;
import com.barqtranslator.core.User;
import com.barqtranslator.core.UserRepository;
import com.barqtranslator.core.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-01T21:57:02.519+03:00")

@Controller
public class UsersApiController implements UsersApi {

    private static final Logger logger = LoggerFactory.getLogger(UsersApiController.class);

    @Autowired
    UserRepository userRepository;

    UserService userService = new UserService();

    @Override
    public ResponseEntity<Void> editStatus(String userId, Status status) {
        if ("out".equals(status.getLogged())) {
            userService.translatorSignOut(userRepository.findOne(userId));
        } else if ("in".equals(status.getLogged())) {
            userService.translatorSignIn(userRepository.findOne(userId));
        }

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> createUser(@RequestBody User user) {
        userRepository.save(user);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<User> getUser(String userId) {
        return new ResponseEntity<User>(userRepository.findOne(userId), HttpStatus.OK);
    }
}
