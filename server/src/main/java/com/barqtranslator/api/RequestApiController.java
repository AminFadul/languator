package com.barqtranslator.api;

import com.barqtranslator.core.Request;
import com.barqtranslator.core.RequestResponse;
import com.barqtranslator.core.User;
import com.barqtranslator.core.UserService;
import org.hibernate.id.GUIDGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.constraints.*;
import javax.validation.Valid;
import java.util.Random;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-01T21:57:02.519+03:00")

@Controller
public class RequestApiController implements RequestApi {

    private static final Logger logger = LoggerFactory.getLogger(RequestApiController.class);

    @Override
    public ResponseEntity<RequestResponse> createRequest(@RequestBody Request request) {
        UserService userService = new UserService();

        String language = request.getLanguage();
        logger.info("Requesting user for language: {}", language);

        if (language == null || language.isEmpty()) {
            return new ResponseEntity<RequestResponse>(HttpStatus.BAD_REQUEST);
        }
        User activeUser = userService.getActiveUser(language);

        if (activeUser == null) {
            return new ResponseEntity<RequestResponse>(HttpStatus.OK);
        } else {
            Random rand = new Random();
            int  n = rand.nextInt(2000) + 1;
            RequestResponse response = new RequestResponse();
            response.setRequestId(String.valueOf(n));
            String id = activeUser.getId();
            response.setTranslatorId(id);

            logger.info("Found translator with id {}", id);

            return new ResponseEntity<RequestResponse>(response, HttpStatus.OK);
        }

    }
}
