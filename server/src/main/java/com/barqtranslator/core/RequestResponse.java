package com.barqtranslator.core;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * RequestResponse
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-01T21:57:02.519+03:00")

public class RequestResponse   {
  @JsonProperty("request_id")
  private String requestId = null;

  @JsonProperty("translator_id")
  private String translatorId = null;

  public RequestResponse requestId(String requestId) {
    this.requestId = requestId;
    return this;
  }

   /**
   * Get requestId
   * @return requestId
  **/
  @ApiModelProperty(value = "")


  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public RequestResponse translatorId(String translatorId) {
    this.translatorId = translatorId;
    return this;
  }

   /**
   * Get translatorId
   * @return translatorId
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getTranslatorId() {
    return translatorId;
  }

  public void setTranslatorId(String translatorId) {
    this.translatorId = translatorId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RequestResponse requestResponse = (RequestResponse) o;
    return Objects.equals(this.requestId, requestResponse.requestId) &&
        Objects.equals(this.translatorId, requestResponse.translatorId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(requestId, translatorId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RequestResponse {\n");
    
    sb.append("    requestId: ").append(toIndentedString(requestId)).append("\n");
    sb.append("    translatorId: ").append(toIndentedString(translatorId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

