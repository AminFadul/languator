package com.barqtranslator.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.*;

public class UserService {

    public UserService() {
        mockUser();
    }

    private static Map<String, Map<String, User>> onlineUserCache = new HashMap<>();

    private void mockUser() {
        User  user = new User();
        user.setId("1343");
        user.setLanguages(Arrays.asList("Bengali"));
        user.setIsTranslator(true);
        user.setName("Ahmed Mohamed");

        if (onlineUserCache.containsKey("Bengali")) {
            onlineUserCache.get("Bengali").put(user.getId(), user);
        } else {
            Map<String, User> userMap = new HashMap<>();
            userMap.put(user.getId(), user);
            onlineUserCache.put("Bengali", userMap);
        }

        user = new User();
        user.setId("1067");
        user.setLanguages(Arrays.asList("Hindi"));
        user.setIsTranslator(true);
        user.setName("Mohammed Noori");

        if (onlineUserCache.containsKey("Hindi")) {
            onlineUserCache.get("Hindi").put(user.getId(), user);
        } else {
            Map<String, User> userMap = new HashMap<>();
            userMap.put(user.getId(), user);
            onlineUserCache.put("Hindi", userMap);
        }
    }

    public User getActiveUser(String language) {
        if (onlineUserCache.containsKey(language) && onlineUserCache.get(language).size() > 0) {
            return onlineUserCache.get(language).entrySet().iterator().next().getValue();
        } else {
            return null;
        }
    }

    public void translatorSignOut(User user) {
        user.getLanguages().forEach(lang -> {
            if (onlineUserCache.containsKey(lang)) {
                onlineUserCache.get(lang).remove(user.getId());
            }
        });
    }


    public void translatorSignIn(User user) {
        user.getLanguages().forEach(lang -> {
            if (onlineUserCache.containsKey(lang)) {
                onlineUserCache.get(lang).put(user.getId(), user);
            } else {
                Map<String, User> userMap = new HashMap<>();
                userMap.put(user.getId(), user);
                onlineUserCache.put(lang, userMap);
            }
        });
    }

}

