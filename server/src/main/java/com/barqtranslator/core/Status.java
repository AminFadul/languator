package com.barqtranslator.core;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Status
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-01T21:57:02.519+03:00")

public class Status   {
  @JsonProperty("logged")
  private String logged = null;

  public Status logged(String logged) {
    this.logged = logged;
    return this;
  }

   /**
   * Get logged
   * @return logged
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getLogged() {
    return logged;
  }

  public void setLogged(String logged) {
    this.logged = logged;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Status status = (Status) o;
    return Objects.equals(this.logged, status.logged);
  }

  @Override
  public int hashCode() {
    return Objects.hash(logged);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Status {\n");
    
    sb.append("    logged: ").append(toIndentedString(logged)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

