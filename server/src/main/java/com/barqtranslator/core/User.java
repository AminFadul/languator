package com.barqtranslator.core;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * User
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-01T21:57:02.519+03:00")

@Entity
public class User   {
  @JsonProperty("id")
  @Id
  @GeneratedValue
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("is_translator")
  private Boolean isTranslator = null;

  @JsonProperty("home_language")
  private String homeLanguage = null;

  @JsonProperty("languages")
  @ElementCollection(targetClass=String.class)
  private List<String> languages = null;

  public User id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public User name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public User isTranslator(Boolean isTranslator) {
    this.isTranslator = isTranslator;
    return this;
  }

   /**
   * Get isTranslator
   * @return isTranslator
  **/
  @ApiModelProperty(value = "")


  public Boolean getIsTranslator() {
    return isTranslator;
  }

  public void setIsTranslator(Boolean isTranslator) {
    this.isTranslator = isTranslator;
  }

  public User homeLanguage(String homeLanguage) {
    this.homeLanguage = homeLanguage;
    return this;
  }

   /**
   * Get homeLanguage
   * @return homeLanguage
  **/
  @ApiModelProperty(value = "")


  public String getHomeLanguage() {
    return homeLanguage;
  }

  public void setHomeLanguage(String homeLanguage) {
    this.homeLanguage = homeLanguage;
  }

  public User languages(List<String> languages) {
    this.languages = languages;
    return this;
  }

  public User addLanguagesItem(String languagesItem) {
    if (this.languages == null) {
      this.languages = new ArrayList<String>();
    }
    this.languages.add(languagesItem);
    return this;
  }

   /**
   * Get languages
   * @return languages
  **/
  @ApiModelProperty(value = "")


  public List<String> getLanguages() {
    return languages;
  }

  public void setLanguages(List<String> languages) {
    this.languages = languages;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return Objects.equals(this.id, user.id) &&
        Objects.equals(this.name, user.name) &&
        Objects.equals(this.isTranslator, user.isTranslator) &&
        Objects.equals(this.homeLanguage, user.homeLanguage) &&
        Objects.equals(this.languages, user.languages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, isTranslator, homeLanguage, languages);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class User {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    isTranslator: ").append(toIndentedString(isTranslator)).append("\n");
    sb.append("    homeLanguage: ").append(toIndentedString(homeLanguage)).append("\n");
    sb.append("    languages: ").append(toIndentedString(languages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

