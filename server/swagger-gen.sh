#!/usr/bin/env bash
swagger-codegen generate \
  -i swagger.yaml \
  -l spring \
  -o generated \
  -c swagger-config.json