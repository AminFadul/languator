//
//  ProfileVC.swift
//  Languator
//
//  Created by Amin Fadul on 01/08/2018.
//  Copyright © 2018 AF. All rights reserved.
//

import UIKit
import XLActionController

class ProfileVC: UIViewController {

    @IBOutlet weak var chooseBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onAdd(_ sender: UIButton) {
        
        
        showActionSheet()
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func showActionSheet(){
        
        let actionController = SpotifyActionController()
        
        actionController.headerData = SpotifyHeaderData(title: "Chose", subtitle: "Chose languge that you want to add", image: #imageLiteral(resourceName: "iconLang"))
        
        
        actionController.addAction(Action(ActionData(title: "english", image: #imageLiteral(resourceName: "iconLang")), style: .default, handler: { action in
            
            self.chooseBtn.setTitle("english", for: .normal)
            
        }))
        actionController.addAction(Action(ActionData(title: "arabic", image:#imageLiteral(resourceName: "iconLang")), style: .default, handler: { action in
            
            self.chooseBtn.setTitle("arabic", for: .normal)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "urdu", image:#imageLiteral(resourceName: "iconLang")), style: .default, handler: { action in
            
            self.chooseBtn.setTitle("urdu", for: .normal)
            
        }))

        actionController.addAction(Action(ActionData(title: "español", image:#imageLiteral(resourceName: "iconLang")), style: .default, handler: { action in
            
            self.chooseBtn.setTitle("español", for: .normal)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "germany", image:#imageLiteral(resourceName: "iconLang")), style: .default, handler: { action in
            
            self.chooseBtn.setTitle("germany", for: .normal)
            
        }))
  
        
        present(actionController, animated: true, completion: nil)
    }

}
