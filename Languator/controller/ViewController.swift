//
//  ViewController.swift
//  Languator
//
//  Created by Amin Fadul on 01/08/2018.
//  Copyright © 2018 AF. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ViewController: UIViewController {
    
    var headers:HTTPHeaders = HTTPHeaders()
    
    @IBAction func onSearch(_ sender: UIButton) {
        SVProgressHUD.show()
        let parameters = ["language": "bengali"]
        self.headers["Content-Type"] = "application/json"
        
        Alamofire.request("http://172.104.129.206:8080/request", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers) .responseJSON { [weak self] response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result

            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
            }
            self?.performSegue(withIdentifier: "call", sender: nil)
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setDefaultMaskType(.black)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "call" {
            let vc = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "CallingViewController") as! CallingViewController
            
            self.present(vc, animated:true, completion:nil)
        }
    }
    

}
